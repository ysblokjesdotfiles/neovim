" In order for this stuff to work you need:
" - rust
" - cmd : rustup component add rls-preview rust-analysis rust-src
" - python-neovim (python3)

set nocompatible


call plug#begin('~/.local/share/nvim/plugged')
"LanguageClient
	Plug 'autozimu/LanguageClient-neovim', {
				\ 'branch' : 'next',
				\ 'do' : 'bash install.sh',
				\}
"File explorer
    Plug 'scrooloose/nerdtree'

" opt. multi-entry
	Plug 'junegunn/fzf'
    Plug 'cloudhead/neovim-fuzzy'

	Plug 'Shougo/deoplete.nvim', {
				\ 'do' : ':UpdateRemotePlugins'
				\}
call plug#end()


" language servers
let g:LanguageClient_serverCommands = {
    \ 'cpp': ['clangd', '--background-index', '--clang-tidy'],
    \ 'hpp': ['clangd', '--background-index', '--clang-tidy'],
    \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
    \ 'python': ['~/.local/bin/pyls'],
    \}

let g:deoplete#enable_at_startup = 1
call deoplete#custom#source('LanguageClient', 
			\ 'min_pattern_length', 2)

function LC_maps()
	if has_key(g:LanguageClient_serverCommands, &filetype)
		nnoremap <buffer> <silent> K :call LanguageClient#textDocument_hover()<CR>
		nnoremap <buffer> <silent> gd :call LanguageClient#textDocument_definition()<CR>
        nnoremap <buffer> <silent> td :call LanguageClient#textDocument_typeDefinition()<CR>
		nnoremap <buffer> <silent> <F2> :call LanguageClient#textDocument_rename()<CR>
    endif
    nnoremap <silent> <C-f> :NERDTreeToggle<CR>
endfunction

function Rust_maps()
    set makeprg=cargo\ build
    "<bar> copen<CR>
    "    nnoremap <silent> 
endfunction

function Generic_maps()
    nnoremap <C-p> :FuzzyOpen<CR>
    nnoremap <C-b> :call Build_with_make() <CR>
    nnoremap <C-n> :call Build_with_ninja()<CR>
endfunction

function Build_with_ninja()
    set makeprg=ninja\ -C\ build
    :make
endfunction

function Build_with_make()
    set makeprg=make
    :make
endfunction

autocmd FileType * call LC_maps()
autocmd FileType * call Generic_maps()
autocmd FileType rust call Rust_maps()
"let g:LanguageClient_loggingFile = '/tmp/lc.log'

set tabstop=4
set softtabstop=0 
set expandtab
set shiftwidth=4
set colorcolumn=81
set number
"hi ColorColumn ctermbg=blue
set autoindent
set smartindent
set showmatch
colorscheme slate
